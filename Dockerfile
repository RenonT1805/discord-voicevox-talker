FROM golang:latest
WORKDIR /go/src/discord-voicevox-talker
ENV GOPATH=/go
ENV DISCORD_VOICEVOX_TALKER_DIR=/go/src/discord-voicevox-talker

RUN apt-get update
RUN apt-get -y install ffmpeg
RUN go install github.com/xo/xo@latest

CMD ["./run.sh"]