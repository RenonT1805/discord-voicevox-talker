package main

import (
	"discord-voicevox-talker/config"
	"discord-voicevox-talker/loggers"

	"github.com/bwmarrin/discordgo"
	"github.com/kardianos/service"

	"database/sql"

	_ "github.com/mattn/go-sqlite3"
)

type app struct {
	exit    chan struct{}
	discord *discordgo.Session
	logger  loggers.Logger
	db      *sql.DB
}

func (a *app) run() {
	db, err := sql.Open("sqlite3", "./dvt.db")
	if err != nil {
		a.logger.Error(err)
		return
	}
	defer db.Close()
	a.db = db

	a.discord, err = discordgo.New(config.GetBotToken())
	if err != nil {
		a.logger.Error(err)
		return
	}
	a.discord.AddHandler(func(s *discordgo.Session, m *discordgo.MessageCreate) {
		messageHnadler(m, a)
	})
	a.discord.Open()

	runAPIServer(a)
}

func (a *app) Start(s service.Service) error {
	a.logger = loggers.GetLogger()
	go a.run()
	return nil
}

func (a *app) Stop(s service.Service) error {
	close(a.exit)
	a.discord.Close()
	return nil
}
