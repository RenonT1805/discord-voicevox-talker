package config

import (
	"os"
	"strings"
)

const CONFIG_FILENAME = "appconfig.json"
const DEFAULT_APP_PATH = "/etc/dvtalker"

// GetAppDir Get app dir
func GetAppDir() string {
	dir := os.Getenv("DISCORD_VOICEVOX_TALKER_DIR")
	if dir == "" {
		return DEFAULT_APP_PATH
	}
	if !strings.HasSuffix(dir, "/") && !strings.HasSuffix(dir, "\\") {
		dir += "/"
	}
	return dir
}

// GetBotToken returns discord bot token
func GetBotToken() string {
	return os.Getenv("VOICEVOX_DISCORD_TALKER_BOTTOKEN")
}
