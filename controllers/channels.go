package controllers

import (
	"context"
	"database/sql"
	"discord-voicevox-talker/models"

	"github.com/bwmarrin/discordgo"
	"github.com/google/uuid"
)

func checkRegistChannel(channelID string, db *sql.DB) (bool, error) {
	row, err := models.ChannelByID(context.Background(), db, channelID)
	if err == sql.ErrNoRows {
		err = nil
	}
	return row != nil, err
}

func registChannel(s *discordgo.Session, m *discordgo.MessageCreate, db *sql.DB, args []string) error {
	commandUsage := generateCommandUsage("channel register", []usageArgs{}, "ボイスチャンネル名")
	if len(args) < 1 {
		if _, err := s.ChannelMessageSend(m.ChannelID, commandUsage); err != nil {
			return err
		}
		return nil
	}

	check, err := checkRegistChannel(m.ChannelID, db)
	if err != nil {
		return err
	}
	if check {
		s.ChannelMessageSend(m.ChannelID, "チャンネルはすでに登録済みです。")
		return nil
	}

	var voiceChannel *discordgo.Channel = nil
	channels, err := s.GuildChannels(m.GuildID)
	if err != nil {
		return err
	}
	for _, channel := range channels {
		if channel.Name == args[0] {
			voiceChannel = channel
		}
	}
	if voiceChannel == nil {
		if _, err := s.ChannelMessageSend(m.ChannelID, "ボイスチャンネル: " + args[0] + " が見つかりません。"); err != nil {
			return err
		}
		return nil
	}

	uuid, err := uuid.NewRandom()
	if err != nil {
		return err
	}
	uuidStr := uuid.String()

	channel := models.Channel{
		ID:             m.ChannelID,
		GuildID:        m.GuildID,
		RequestID:      uuidStr,
		VoiceChannelID: voiceChannel.ID,
	}
	err = channel.Insert(context.Background(), db)
	if err != nil {
		return err
	}
	s.ChannelMessageSend(m.ChannelID, "チャンネルの登録に成功しました。\n"+"リクエストID: "+uuidStr)
	return nil
}

func unregistChannel(s *discordgo.Session, m *discordgo.MessageCreate, db *sql.DB) error {
	channel, err := models.ChannelByID(context.Background(), db, m.ChannelID)
	if err != nil {
		return err
	}
	channel.Delete(context.Background(), db)
	s.ChannelMessageSend(m.ChannelID, "チャンネルの登録を解除しました。")
	return nil
}

func getChannelInfo(s *discordgo.Session, m *discordgo.MessageCreate, db *sql.DB) error {
	channel, err := models.ChannelByID(context.Background(), db, m.ChannelID)
	if err != nil {
		return err
	}
	content := "このチャンネルの登録情報\n"
	content += "ギルドID: " + channel.GuildID + "\n"
	content += "チャンネルID: " + channel.ID + "\n"
	content += "リクエストID: " + channel.RequestID
	s.ChannelMessageSend(m.ChannelID, content)
	return nil
}

func ChannelsMessageHandler(s *discordgo.Session, m *discordgo.MessageCreate, db *sql.DB, args []string) error {
	commandUsage := generateCommandUsage("channel", []usageArgs{
		{
			name:        "register",
			description: "このチャンネルをBotに登録する。",
		},
		{
			name:        "unregister",
			description: "このチャンネルの登録を解除する。",
		},
		{
			name:        "info",
			description: "このチャンネルの登録情報を表示する。",
		},
	}, "")

	if len(args) == 0 {
		if _, err := s.ChannelMessageSend(m.ChannelID, commandUsage); err != nil {
			return err
		}
		return nil
	}

	switch args[0] {
	case "register":
		return registChannel(s, m, db, args[1:])
	case "unregister":
		return unregistChannel(s, m, db)
	case "info":
		return getChannelInfo(s, m, db)
	default:
		if _, err := s.ChannelMessageSend(m.ChannelID, commandUsage); err != nil {
			return err
		}
		return nil
	}
}
