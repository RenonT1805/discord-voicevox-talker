package controllers

import (
	"context"
	"database/sql"
	"discord-voicevox-talker/models"
	"strconv"

	"github.com/bwmarrin/discordgo"
)

func addCharacter(s *discordgo.Session, m *discordgo.MessageCreate, db *sql.DB, args []string) error {
	commandUsage := generateCommandUsage("character add", []usageArgs{}, "キャラクター名 スピーカーID")
	if len(args) < 2 {
		if _, err := s.ChannelMessageSend(m.ChannelID, commandUsage); err != nil {
			return err
		}
		return nil
	}

	speakerID, err := strconv.Atoi(args[1])
	if err != nil {
		if _, err := s.ChannelMessageSend(m.ChannelID, "スピーカーIDは数値で入力してください"); err != nil {
			return err
		}
		return nil
	}

	_, err = models.SpeakerByID(context.Background(), db, speakerID)
	if err == sql.ErrNoRows {
		if _, err := s.ChannelMessageSend(m.ChannelID, "存在しないスピーカーIDです。\n+speakers コマンドでスピーカーの一覧を確認してください。"); err != nil {
			return err
		}
		return nil
	}

	stmt, err := db.Prepare("SELECT * FROM characters WHERE name = ? AND channel_id = ?")
	if err != nil {
		return err
	}
	err = stmt.QueryRow(args[0], m.ChannelID).Scan()
	if err != sql.ErrNoRows {
		if _, err := s.ChannelMessageSend(m.ChannelID, "キャラクター名: "+args[0]+" は既に存在しています。"); err != nil {
			return err
		}
		return nil
	}

	chara := models.Character{
		Name:      args[0],
		SpeakerID: speakerID,
		ChannelID: m.ChannelID,
	}
	err = chara.Insert(context.Background(), db)
	if err != nil {
		return err
	}
	s.ChannelMessageSend(m.ChannelID, "キャラクターの登録に成功しました。")
	return nil
}

func deleteCharacter(s *discordgo.Session, m *discordgo.MessageCreate, db *sql.DB, args []string) error {
	commandUsage := generateCommandUsage("character delete", []usageArgs{}, "キャラクター名")
	if len(args) < 1 {
		if _, err := s.ChannelMessageSend(m.ChannelID, commandUsage); err != nil {
			return err
		}
		return nil
	}

	stmt, err := db.Prepare("SELECT id FROM characters WHERE name = ?")
	if err != nil {
		return err
	}
	var id int
	err = stmt.QueryRow(args[0]).Scan(&id)
	if err == sql.ErrNoRows {
		if _, err := s.ChannelMessageSend(m.ChannelID, "キャラクター名: "+args[0]+" は存在しません。"); err != nil {
			return err
		}
		return nil
	} else if err != nil {
		return err
	}

	character, err := models.CharacterByID(context.Background(), db, id)
	if err != nil {
		return err
	}
	character.Delete(context.Background(), db)

	s.ChannelMessageSend(m.ChannelID, "キャラクターの削除に成功しました。")
	return nil
}

func listingCharacter(s *discordgo.Session, m *discordgo.MessageCreate, db *sql.DB) error {
	rows, err := db.Query("SELECT name, speaker_id FROM characters WHERE channel_id = ?", m.ChannelID)
	if err != nil {
		return err
	}
	var characters []struct {
		name    string
		speaker string
	}
	speakers, err := getSpeakers(db)
	if err != nil {
		return err
	}

	for rows.Next() {
		var name string
		var speakerID int
		err = rows.Scan(&name, &speakerID)
		if err != nil {
			return err
		}
		var speakerName = "スピーカー設定なし"
		for _, speaker := range speakers {
			if speaker.ID == speakerID {
				speakerName = speaker.Name
			}
		}
		characters = append(characters, struct {
			name    string
			speaker string
		}{
			name:    name,
			speaker: speakerName,
		})
	}
	str := "キャラクターの一覧：\n"
	for _, character := range characters {
		str += "名前: " + character.name + "    [スピーカー：" + character.speaker + "]\n"
	}
	s.ChannelMessageSend(m.ChannelID, str)
	return err
}

func CharactersMessageHandler(s *discordgo.Session, m *discordgo.MessageCreate, db *sql.DB, args []string) error {
	commandUsage := generateCommandUsage("character", []usageArgs{
		{
			name:        "add",
			description: "キャラクターをBotに登録する。",
		},
		{
			name:        "delete",
			description: "登録したキャラクターを削除する。",
		},
		{
			name:        "list",
			description: "登録したキャラクターの一覧を表示する",
		},
	}, "")

	if len(args) == 0 {
		if _, err := s.ChannelMessageSend(m.ChannelID, commandUsage); err != nil {
			return err
		}
		return nil
	}

	switch args[0] {
	case "add":
		return addCharacter(s, m, db, args[1:])
	case "delete":
		return deleteCharacter(s, m, db, args[1:])
	case "list":
		return listingCharacter(s, m, db)
	default:
		if _, err := s.ChannelMessageSend(m.ChannelID, commandUsage); err != nil {
			return err
		}
		return nil
	}
}
