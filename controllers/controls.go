package controllers

import (
	"context"
	"database/sql"
	"discord-voicevox-talker/models"

	"github.com/bwmarrin/discordgo"
)

func Join(s *discordgo.Session, m *discordgo.MessageCreate, db *sql.DB) error {
	_, exist := s.VoiceConnections[m.GuildID]
	if exist {
		return nil
	}
	channel, err := models.ChannelByID(context.Background(), db, m.ChannelID)
	if err != nil {
		return err
	}
	_, err = s.ChannelVoiceJoin(m.GuildID, channel.VoiceChannelID, false, false)
	return err
}

func Leave(s *discordgo.Session, m *discordgo.MessageCreate) error {
	_, exist := s.VoiceConnections[m.GuildID]
	if !exist {
		return nil
	}
	err := s.VoiceConnections[m.GuildID].Disconnect()
	if err != nil {
		return err
	}
	return nil
}
