package controllers

import (
	"context"
	"database/sql"
	"discord-voicevox-talker/models"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strconv"

	"github.com/bwmarrin/dgvoice"
	"github.com/bwmarrin/discordgo"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

func speak(connection *discordgo.VoiceConnection, speakerID int, str string) error {
	uuid, err := uuid.NewRandom()
	if err != nil {
		return err
	}
	filename := "tmp/" + uuid.String()

	client := new(http.Client)
	request, err := http.NewRequest("POST", "http://voicevox_engine:50021/audio_query?speaker="+strconv.Itoa(speakerID)+"&text="+url.QueryEscape(str), nil)
	if err != nil {
		return err
	}
	queryResponse, err := client.Do(request)
	if err != nil {
		return err
	}
	defer queryResponse.Body.Close()

	request, err = http.NewRequest("POST", "http://voicevox_engine:50021/synthesis?speaker="+strconv.Itoa(speakerID), queryResponse.Body)
	request.Header.Set("Content-Type", "application/json")
	if err != nil {
		return err
	}
	audioResponse, err := client.Do(request)
	if err != nil {
		return err
	}
	defer audioResponse.Body.Close()

	data, err := ioutil.ReadAll(audioResponse.Body)
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(filename, data, 0666)
	if err != nil {
		return err
	}

	connection.Speaking(true)
	send := make(chan []int16, 2)
	defer close(send)
	defer connection.Speaking(false)

	stop := make(chan bool, 1)
	dgvoice.PlayAudioFile(connection, filename, stop)
	return os.Remove(filename)
}

func SpeakFromChats(c *gin.Context, s *discordgo.Session, db *sql.DB) error {
	requestBinary, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		return err
	}
	var chats []models.Chat
	err = json.Unmarshal(requestBinary, &chats)
	if err != nil {
		return err
	}

	for _, chat := range chats {
		stmt, err := db.Prepare("SELECT id, guild_id FROM channels WHERE request_id = ?")
		if err != nil {
			return err
		}
		var guildID string
		var channelID string
		err = stmt.QueryRow(chat.RequestID).Scan(&channelID, &guildID)
		if err == sql.ErrNoRows {
			c.JSON(401, gin.H{})
			return nil
		} else if err != nil {
			return err
		}

		stmt, err = db.Prepare("SELECT speaker_id FROM characters WHERE name = ? AND channel_id = ?")
		if err != nil {
			return err
		}
		var speakerID int
		err = stmt.QueryRow(chat.Author, channelID).Scan(&speakerID)
		if err == sql.ErrNoRows {
			c.JSON(404, gin.H{
				"message": "character name: " + chat.Author + " is not found",
			})
			return nil
		} else if err != nil {
			return err
		}

		connection, exist := s.VoiceConnections[guildID]
		if !exist {
			c.JSON(400, gin.H{
				"message": "Bot is not join voice channel yet",
			})
			return nil
		}

		speak(connection, speakerID, chat.Text)
	}

	c.JSON(200, gin.H{})
	return nil
}

func Echo(s *discordgo.Session, m *discordgo.MessageCreate, db *sql.DB, args []string) error {
	commandUsage := generateCommandUsage("echo", []usageArgs{}, "スピーカーID 発話文字列")
	if len(args) < 2 {
		if _, err := s.ChannelMessageSend(m.ChannelID, commandUsage); err != nil {
			return err
		}
		return nil
	}

	speakerID, err := strconv.Atoi(args[0])
	if err != nil {
		if _, err := s.ChannelMessageSend(m.ChannelID, "スピーカーIDは数値で入力してください"); err != nil {
			return err
		}
		return nil
	}

	_, err = models.SpeakerByID(context.Background(), db, speakerID)
	if err == sql.ErrNoRows {
		if _, err := s.ChannelMessageSend(m.ChannelID, "存在しないスピーカーIDです。\n+speakers コマンドでスピーカーの一覧を確認してください。"); err != nil {
			return err
		}
		return nil
	}

	connection, exist := s.VoiceConnections[m.GuildID]
	if !exist {
		s.ChannelMessageSend(m.ChannelID, "ボイスチャンネルに接続していません。")
		return nil
	}

	return speak(connection, speakerID, args[1])
}
