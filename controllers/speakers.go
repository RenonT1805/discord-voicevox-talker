package controllers

import (
	"database/sql"
	"discord-voicevox-talker/models"
	"strconv"

	"github.com/bwmarrin/discordgo"
)

func getSpeakers(db *sql.DB) ([]models.Speaker, error) {
	rows, err := db.Query("SELECT * FROM speakers")
	if err != nil {
		return []models.Speaker{}, err
	}
	var result []models.Speaker
	for rows.Next() {
		var id int
		var name string
		err = rows.Scan(&id, &name)
		if err != nil {
			return []models.Speaker{}, err
		}
		result = append(result, models.Speaker{
			ID:   id,
			Name: name,
		})
	}
	return result, err
}

func GetSpeakerList(s *discordgo.Session, m *discordgo.MessageCreate, db *sql.DB) error {
	speakers, err := getSpeakers(db)
	if err != nil {
		return err
	}
	str := "スピーカーの一覧：\n"
	for _, speaker := range speakers {
		str += "ID: " + strconv.Itoa(speaker.ID) + "  " + speaker.Name + "\n"
	}
	s.ChannelMessageSend(m.ChannelID, str)
	return nil
}
