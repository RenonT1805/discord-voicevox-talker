package controllers

type usageArgs struct {
	name        string
	description string
}

func generateCommandUsage(commandName string, commands []usageArgs, valueDescription string) string {
	result := "使い方:\n  " + commandName
	if valueDescription != "" {
		result += " " + valueDescription
	}
	if len(commands) > 0 {
		result += " [コマンド]\n\nコマンド一覧:\n"
		for _, command := range commands {
			result += "  " + command.name + "  " + command.description + "\n"
		}
	}
	return result
}
