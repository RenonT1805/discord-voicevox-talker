CREATE TABLE channels (
  id TEXT NOT NULL PRIMARY KEY,
  guild_id TEXT NOT NULL,
  request_id TEXT NOT NULL,
  voice_channel_id TEXT NOT NULL
);