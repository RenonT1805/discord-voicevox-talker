CREATE TABLE characters (
  id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  channel_id TEXT NOT NULL,
  name TEXT NOT NULL,
  speaker_id INTEGER NOT NULL,
  speed_scale NUMERIC,
  pitch_scale NUMERIC,
  intonation_scale NUMERIC,
  volume_scale NUMERIC,
  pre_phoneme_length NUMERIC,
  post_phoneme_length NUMERIC,
  output_sampling_rate INTEGER,
  outputStereo TEXT
);