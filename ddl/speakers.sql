CREATE TABLE speakers (
  id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  name TEXT NOT NULL
);

INSERT INTO speakers 
  (id, name) 
VALUES 
  (2, '四国めたん（ノーマル）'),
  (0, '四国めたん（あまあま）'),
  (6, '四国めたん（ツンツン）'),
  (4, '四国めたん（セクシー）'),
  (3, 'ずんだもん（ノーマル）'),
  (1, 'ずんだもん（あまあま）'),
  (7, 'ずんだもん（ツンツン）'),
  (5, 'ずんだもん（セクシー）'),
  (8, '春日部つむぎ（ノーマル）'),
  (10, '雨晴はう（ノーマル）'),
  (9, '波音リツ（ノーマル）'),
  (11, '玄野武宏（ノーマル）'),
  (12, '白上虎太郎（ノーマル）'),
  (13, '青山龍星（ノーマル）'),
  (14, '冥鳴ひまり（ノーマル）'),
  (16, '九州そら（ノーマル）'),
  (15, '九州そら（あまあま）'),
  (18, '九州そら（ツンツン）'),
  (17, '九州そら（セクシー）'),
  (19, '九州そら（ささやき）'),
  (20, 'モチノ・キョウコ（ノーマル）');