package loggers

import (
	"log"

	"github.com/kardianos/service"
)

type Logger = service.Logger

var logger Logger

// Initialize : logger initialize
func Initialize(s service.Service) error {
	var err error
	errs := make(chan error, 5)
	logger, err = s.Logger(errs)
	if err != nil {
		return err
	}

	go func() {
		for {
			err := <-errs
			if err != nil {
				log.Print(err)
			}
		}
	}()
	return nil
}

// GetLogger : get logger
func GetLogger() Logger {
	return logger
}
