build:
	docker-compose build

up:
	docker-compose up

init:
	docker-compose exec discord-voicevox-talker bash /go/src/discord-voicevox-talker/init.sh

bash:
	docker-compose exec discord-voicevox-talker bash
	
stop:
	docker-compose stop
	
down:
	docker-compose down
	
sdown:
	docker-compose down --rmi all --volumes --remove-orphans
