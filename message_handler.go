package main

import (
	"context"
	"database/sql"
	"discord-voicevox-talker/controllers"
	"discord-voicevox-talker/models"
	"encoding/csv"
	"strings"

	"github.com/bwmarrin/discordgo"
)

func messageHandlerErrorToLog(err error, a *app, m *discordgo.MessageCreate) {
	if err != nil {
		a.discord.ChannelMessageSend(m.ChannelID, "サーバーでエラーが発生しました")
		a.logger.Error(err)
	}
}

func parseMessage(argstr string) ([]string, error) {
	argstr = strings.TrimSpace(argstr)
	if len(argstr) == 0 {
		return []string{}, nil
	}

	reader := csv.NewReader(strings.NewReader(argstr))
	reader.Comma = ' '
	out, err := reader.Read()
	if err != nil {
		return nil, err
	}
	return out, nil
}

func checkRegistChannel(channelID string, db *sql.DB) (bool, error) {
	row, err := models.ChannelByID(context.Background(), db, channelID)
	if err == sql.ErrNoRows {
		err = nil
	}
	return row != nil, err
}

func getSpeakers(db *sql.DB) ([]models.Speaker, error) {
	rows, err := db.Query("SELECT * FROM speakers")
	if err != nil {
		return []models.Speaker{}, err
	}
	var result []models.Speaker
	for rows.Next() {
		var id int
		var name string
		err = rows.Scan(&id, &name)
		if err != nil {
			return []models.Speaker{}, err
		}
		result = append(result, models.Speaker{
			ID:   id,
			Name: name,
		})
	}
	return result, err
}

func messageHnadler(m *discordgo.MessageCreate, a *app) {
	if m.Author.Bot {
		return
	}
	check, err := checkRegistChannel(m.ChannelID, a.db)
	if err != nil {
		messageHandlerErrorToLog(err, a, m)
	}
	if !check && !strings.HasPrefix(m.Message.Content, "+channel register") {
		return
	}

	args, err := parseMessage(m.Message.Content)
	if err != nil {
		messageHandlerErrorToLog(err, a, m)
	}
	if len(args) == 0 {
		return
	}
	a.logger.Info(args)
	switch args[0] {
	case "+channel":
		messageHandlerErrorToLog(controllers.ChannelsMessageHandler(a.discord, m, a.db, args[1:]), a, m)
	case "+character":
		messageHandlerErrorToLog(controllers.CharactersMessageHandler(a.discord, m, a.db, args[1:]), a, m)
	case "+speakers":
		messageHandlerErrorToLog(controllers.GetSpeakerList(a.discord, m, a.db), a, m)
	case "+join":
		messageHandlerErrorToLog(controllers.Join(a.discord, m, a.db), a, m)
	case "+leave":
		messageHandlerErrorToLog(controllers.Leave(a.discord, m), a, m)
	case "+echo":
		messageHandlerErrorToLog(controllers.Echo(a.discord, m, a.db, args[1:]), a, m)
	default:
	}
}
