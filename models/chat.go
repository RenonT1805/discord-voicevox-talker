package models

type Chat struct {
	Text      string `json:"text"`
	Author    string `json:"author"`
	RequestID string `json:"requestID"`
}
