package main

import (
	"discord-voicevox-talker/controllers"
	"os"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func apiErrorToLog(err error, a *app, c *gin.Context) {
	if err != nil {
		c.JSON(500, gin.H{})
		a.logger.Error(err)
	}
}

func newRouter() *gin.Engine {
	r := gin.Default()
	r.Use(cors.New(cors.Config{
		AllowOrigins: []string{
			"https://ccfolia.com",
		},
		AllowMethods: []string{
			"POST",
		},
		AllowHeaders: []string{
			"Access-Control-Allow-Credentials",
			"Access-Control-Allow-Headers",
			"Content-Type",
			"Content-Length",
			"Accept-Encoding",
			"Authorization",
		},
	}))
	return r
}

func runAPIServer(a *app) {
	r := newRouter()

	r.POST("/speak", func(c *gin.Context) {
		apiErrorToLog(controllers.SpeakFromChats(c, a.discord, a.db), a, c)
	})

	port := os.Getenv("VOICEVOX_DISCORD_TALKER_PORT")
	if port == "" {
		port = "8270"
	}
	r.Run(":" + port)
}
