package main

import (
	"database/sql"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"

	_ "github.com/mattn/go-sqlite3"
)

func dirwalk(dir string) []string {
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		panic(err)
	}

	var paths []string
	for _, file := range files {
		if file.IsDir() {
			paths = append(paths, dirwalk(filepath.Join(dir, file.Name()))...)
			continue
		}
		paths = append(paths, filepath.Join(dir, file.Name()))
	}

	return paths
}

func main() {
	os.Remove("./dvt.db")
	db, err := sql.Open("sqlite3", "./dvt.db")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	paths := dirwalk("./ddl")
	for _, path := range paths {
		log.Printf("execute sql: %s", path)
		bytes, err := ioutil.ReadFile(path)
		if err != nil {
			log.Fatal(err)
		}
		ddl := string(bytes)
		_, err = db.Exec(ddl)
		if err != nil {
			log.Printf("%q: %s\n", err, ddl)
			return
		}
		log.Printf("success: %s", path)
	}
}
